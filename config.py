#!/usr/bin/env python3
import argparse
import json
from pprint import pprint
import os
import sys

WORKING_DIRECTORY = os.path.dirname(os.path.abspath(__file__))
CONFIG_FILE_PATH = "{}/config.json".format(WORKING_DIRECTORY)

class Configuration(object):
    def __init__(self):
        try:
            with open(CONFIG_FILE_PATH) as f:
                self.data = json.load(f) 
        except Exception as e:
            print("Failed to parse configuration file: {}".format(str(e)))
    
    
    def get(self, key=None):
        if key is None:
            return self.data
        else:
            try:
                return self.data[key]
            except Exception as e:
                print("Failed to get {} in config because: {}".format(key, str(e)))
                return None

# Our configuration interface
conf = Configuration()