#!/usr/bin/python

import argparse
import cv2
import numpy as np
import sys
import config
import pickle
import os
import json
# Parse command line arguments
parser = argparse.ArgumentParser(
        description="Calculates bounding box for an input heatmap",
        prog="main.py",
        usage='%(prog)s [options] <message>'
        )

# Setup our parser
parser.add_argument("-i", "--image", required=True, help="The input heatmap to calculate a bbox for")
parser.add_argument("-o", "--output", default=None, help="The path to the output file with the bbox applied")
parser.add_argument("-d", "--debug", action='store_true', help="Display the images for debugging purposes")
parser.add_argument("-p", "--pickle", action='store_true', help="Use a pickle image")
parser.add_argument("--original", default=None, help="Image that heatmap was generated from")
parser.add_argument("--label", default="Animal", help="Label for the bounding box")

def resize_image(input, original_path):
    """Resizes a heatmap to match original image
    """
    orig_image = cv2.imread(original_path)
    width = int(orig_image.shape[1])
    height = int(orig_image.shape[0])
    dim = (width, height)
    resized_input = cv2.resize(input, dim)
    return resized_input


def threshold(input, min=150, max=255):
    """Thresholds a gray scale image
    """
    th, thresh_image = cv2.threshold(input, min, max, cv2.THRESH_BINARY)
    return thresh_image

def flood_fill(input):
    """Performs flood fill operation on grayscale image
    """
    # Copy the thresholded image.
    im_floodfill = input.copy()
    
    # Mask used to flood filling.
    # Notice the size needs to be 2 pixels than the image.
    h, w = input.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)
    
    # Floodfill from point (0, 0)
    cv2.floodFill(im_floodfill, mask, (0,0), 255);

    # Invert floodfilled image
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    
    # Combine the two images to get the foreground.
    im_out = input | im_floodfill_inv
    return im_out

def close_holes(input, kernel_size=15):
    """Morph close to an image
    """
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    im_out = cv2.morphologyEx(input, cv2.MORPH_OPEN, kernel)
    im_out = cv2.morphologyEx(im_out, cv2.MORPH_CLOSE, kernel)
    return im_out

def detect_contours(input):
    """Detect contours in an image, returns contours
    """
    im, contours, hierarchy = cv2.findContours(input, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return contours

def get_bounding_rects(contours):
    """Get bounding rects from a set of countours
    """
    rects = []
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        rects.append((x, y, w, h))
    return rects

def filter_bounding_rects(rects, input, thresh):
    """Filter bounding boxes by max value inside them
    """
    filtered_rects = []
    for rect in rects:
        x,y,w,h = rect
        if np.max(input[y:y+h,x:x+w]) >= thresh:
            filtered_rects.append(rect)
    return filtered_rects

def heatmap_to_bbox(input, display=False):
    """Takes in a heatmap and produces bbox coordinates

    Return
        list of tuples 
    """
    # Convert to gray scale
    if len(input.shape) > 2:
        gray_image = cv2.cvtColor(input, cv2.COLOR_BGR2GRAY)
    else:
        gray_image = input

    # Threshold
    threshold_image = threshold(gray_image, config.conf.get("threshold_min"), config.conf.get("threshold_max"))

    # Floodfill
    flood_fill_image = flood_fill(threshold_image)

    # Closing
    closed_image = close_holes(flood_fill_image, config.conf.get("close_kernel"))

    # contour detection
    contours = detect_contours(closed_image)
    if display:
        input = cv2.drawContours(input, contours, -1, (0, 255, 0), 4)
        cv2.namedWindow("Detected Contours", cv2.WINDOW_NORMAL)
        cv2.imshow("Detected Contours", input)

    bboxes = get_bounding_rects(contours)
    bboxes = filter_bounding_rects(bboxes, gray_image,config.conf.get("threshold_filt"))
    return bboxes

def save_detections(image_file, bboxes, out_path, **kwargs):
    """Saves the bboxes generated to json
    """
    output = {'detections':[]}
    id = 0
    fname = os.path.splitext(os.path.basename(image_file))[0] + '.png'
    for bbox in bboxes:
        x,y,w,h = bbox
        item = {
            'image_file': fname,
            'id': str(id),
            'prob': '1.0',
            'species': kwargs['species'],
            'subspecies': kwargs['subspecies'],
            'type': 'box',
            'x': str(x),
            'y': str(y),
            'w': str(w),
            'h': str(h)
        }
        output['detections'].append(item)
        id += 1
    
    # Convert to json
    output_json = json.dumps(output)
    # Get the base filename
    #basename = os.path.splitext(image_file)[0]
    with open("{}.json".format(os.path.join(out_path,os.path.splitext(fname)[0])), 'w') as f:
        json.dump(output, f,  sort_keys=False, indent=4)
        
def main():
    """Main loop
    """
    arguments = parser.parse_args()

    if arguments.image:
        image_path = arguments.image
        print("Loading heatmap: {}".format(image_path))

        if arguments.pickle:
            file = open(arguments.image, 'rb')
            image = pickle.load(file)
            if np.max(image) <= 1.0:
                image = np.array((1-image) * 255, dtype=np.uint8)
        else:
            image = cv2.imread(image_path)

        if arguments.original:
            image = resize_image(
                    image, 
                    arguments.original + os.path.splitext(os.path.basename(image_path))[0] + ".png")
        bboxes = heatmap_to_bbox(image, display=arguments.debug)

        print("Got {} bounding boxes".format(len(bboxes)))
        for bbox in bboxes:
            x,y,w,h = bbox
            cv2.rectangle(image, (x,y), (x+w, y+h), (0, 255, 0), 2)

        if arguments.debug:
            cv2.namedWindow("ROI", cv2.WINDOW_NORMAL)
            cv2.imshow("ROI", image)   
            cv2.waitKey(0)
        
        if arguments.output:
            filename = os.path.join(
                    arguments.output,
                    os.path.splitext(os.path.basename(arguments.image))[0])
            print("Saving image to: {}_box.png".format(filename))
            cv2.imwrite("{}.png".format(filename), image)

            print("Saving bbox coordinates to {}.json".format(filename))

            save_detections(
                    arguments.image, 
                    bboxes, 
                    arguments.output, 
                    species=arguments.label, 
                    subspecies="")


if __name__ == '__main__':
    main()
