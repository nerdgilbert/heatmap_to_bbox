#!/bin/bash
PKL_DIR=/mnt/md0/ben/Grad_CAM_plus_plus/output/Midwater_512_Final/Aegina/
IMAGE_DIR=/mnt/md0/Projects/FathomNet/Data_Files/vars_images_metadata/vars_images/Aegina/

for fname in "$PKL_DIR"*.pkl
do
    echo $fname 
    python main.py -i ${fname} --output ${PKL_DIR}bbox/ --pickle --original ${IMAGE_DIR}
done
