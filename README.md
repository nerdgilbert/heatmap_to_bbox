# Heatmap to Bounding Box

## WIP


## Usage

The simplest usage: 

`python3 main.py --input [path to heatmap]`

Save to disk and visualize

`python3 main.py --input [path to heatmap] --output [file name to save output heatmap and bbox coordinates] --debug`

## Example output
![image](test.png)